package godi

func ValueProvider[T any](value T) func() T {
	return func() T {
		return value
	}
}

func ValueConsumer[T any]() func(T) T {
	return func(value T) T {
		return value
	}
}
