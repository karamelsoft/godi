# GODI

A very simple reflection-based dependency waiting for the generics to become a real thing injection 
library for Golang.

## Introduction

Why having dependency in Golang? It was design to be simple, to stay away from troubles and bullshits! Yeah
I can't agree more, but sometimes you can keep it simple and easy to build at the same time! That is where
Godi enters the game.

## How to get it?
```shell
go get gitlab.com/karamelsoft/godi
```

## Context

A context to rule them all! You know the common practice to drag the context from the very beginning to the
end of your tree, godi follows the same principle. Indeed, the dependency is done using the very same context 
which keeps track of your providers.

## Usage

###Creation

```go
ctx := context.Background() // the same same way to create your context
```

### Add a provider

Because Golang reflection is a bit shitty we have to use functions,
which is not so bad because we can have a lazy loading behaviour!

```go
godi.Add(ctx, func() MySuperInterface { 
	///initialize...
	return mySuperImplementation 
})
// or 
godi.Add(ctx, func(ctx context.Context, dependency MySuperDependency) MySuperInterface { 
	///initialize...
	return NewSuperImplementation(dependency) 
})
// or 
godi.Add(ctx, func(ctx context.Context, dependency MySuperDependency) (MySuperInterface, error) { 
	///initialize...
	return NewSuperImplementation(dependency) 
})
// or 
godi.Add(ctx, 
	func(...) ... {},
	func(...) ... {},
	func(...) ... {},
	func(...) ... {},
)

```

### Get a service

```go
mySuperService := godi.GetOrCrash[MySuperService](ctx)
```

### Consumer

Consuming means we do not return anything or just an error:

```go
godi.Consume(ctx, func(target MySuperInterface) {
	target.DoSomethingHere()
})
```
```go
godi.ConsumeUnsafe(ctx, func(target MySuperInterface) error {
	return target.DoSomethingHere() // can fail
})
```

### Execute

Executing means we return a value or a tuple ```(value, error)```:

```go
result := godi.Execute(ctx, func(target MySuperInterface) MyResult {
	return target.Result()
})
// or simply
dep := godi.Execute(ctx, func(target MySuperInterface) MySuperInterface {
	return target
})
```

```go
result, err := godi.ExecuteUnsafe(ctx, func(target MySuperInterface) (MyResult, error) {
	return target.Result(), nil
})
```


