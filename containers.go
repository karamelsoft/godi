package godi

import (
	"context"
	"fmt"
	"reflect"
	"sync"
)

type diServiceKey string

func newContainers() *containers {
	return &containers{
		lock:       &sync.RWMutex{},
		containers: make(map[diServiceKey]container),
	}
}

type containers struct {
	lock       *sync.RWMutex
	containers map[diServiceKey]container
}

func (cs *containers) Close() error {
	cs.lock.RLock()
	defer cs.lock.RUnlock()

	for _, container := range cs.containers {
		if err := container.Close(); err != nil {
			return err
		}
	}

	return nil
}

func (cs *containers) Add(providers ...any) {
	cs.lock.Lock()
	defer cs.lock.Unlock()

	for _, provider := range providers {
		cs.containers[providerKey(provider)] = newLazyContainer(cs, provider)
	}
}

func (cs *containers) Execute(ctx context.Context, consumer any) ([]any, error) {
	cs.lock.RLock()
	defer cs.lock.RUnlock()

	keys, function := consumerBehaviour(consumer)

	services, err := cs.services(ctx, keys)
	if err != nil {
		return nil, err
	}

	return function(services), nil
}

func (cs *containers) ExecuteOrCrash(ctx context.Context, consumer any) []any {
	results, err := cs.Execute(ctx, consumer)
	if err != nil {
		panic(err)
	}

	return results
}

func (cs *containers) services(ctx context.Context, keys []diServiceKey) ([]any, error) {
	var values []any
	for _, key := range keys {
		if key == "context.Context" {
			values = append(values, ctx)
		} else {
			valueContainer := cs.containers[key]
			if valueContainer == nil {
				return nil, fmt.Errorf("no provider found for type: %v", key)
			}
			values = append(values, valueContainer.(container).Get(ctx))
		}
	}

	return values, nil
}

func providerKey(provider any) diServiceKey {
	providerType := reflect.TypeOf(provider)
	switch {
	case providerType.Kind() != reflect.Func:
		panic(fmt.Errorf("wrong provider type: %v", providerType))
	case providerType.NumOut() == 1:
		fallthrough
	case providerType.NumOut() == 2 && providerType.Out(1).Implements(reflect.TypeFor[error]()):
		return diServiceKey(providerType.Out(0).String())
	default:
		panic(fmt.Errorf("wrong provider result, should be one value or 2 with an error: %v", providerType))
	}
}

func consumerBehaviour(consumer any) ([]diServiceKey, func([]any) []any) {
	consumerType := reflect.TypeOf(consumer)
	consumerValue := reflect.ValueOf(consumer)

	switch {
	case consumerType.Kind() == reflect.Func:
		return argumentKeys(consumerType), func(inputs []any) []any {
			return values(consumerValue.Call(reflectValues(inputs)))
		}
	default:
		return []diServiceKey{diServiceKey(fmt.Sprintf("%v", consumerValue.Interface()))},
			func(values []any) []any {
				return values
			}
	}
}

func argumentKeys(_type reflect.Type) []diServiceKey {
	keys := make([]diServiceKey, 0, _type.NumIn())
	for i := 0; i < _type.NumIn(); i++ {
		keys = append(keys, diServiceKey(_type.In(i).String()))
	}
	return keys
}

func reflectValues(inputs []any) []reflect.Value {
	values := make([]reflect.Value, 0, len(inputs))
	for _, input := range inputs {
		values = append(values, reflect.ValueOf(input))
	}
	return values
}

func values(inputs []reflect.Value) []any {
	interfaces := make([]any, 0, len(inputs))
	for _, input := range inputs {
		interfaces = append(interfaces, input.Interface())
	}
	return interfaces
}
