package godi

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestGet(t *testing.T) {
	t.Run("get context.Context on unitialized context", func(t *testing.T) {
		defer func() {
			if r := recover(); r == nil {
				assert.Fail(t, "expected panic, but none detected")
			}
		}()
		ctx := context.Background()
		_, _ = Get[context.Context](ctx)
	})
	t.Run("add simple provider then get it then close", func(t *testing.T) {
		closed := false
		ctx := context.Background()
		ctx = Add(ctx, func() PrintService {
			return NewPrintService(func() {
				closed = true
			})
		})

		service, err := Get[PrintService](ctx)
		assert.NoError(t, err)

		message := service.Print("hello")

		assert.Equal(t, "hello", message)
		assert.NoError(t, Close(ctx))
		assert.True(t, closed)
	})
	t.Run("add many providers then get a complex one then close", func(t *testing.T) {
		printClosed := false
		l33dClosed := false
		logClosed := false

		ctx := context.Background()
		ctx = Add(ctx,
			func() PrintService {
				return NewPrintService(func() {
					printClosed = true
				})
			},
			func() *l33dService {
				return NewL33dService(func() {
					l33dClosed = true
				})
			},
			func(ctx context.Context, ps PrintService, hs *l33dService) LogService {
				return NewLogService(ps, hs, func() {
					logClosed = true
				})
			},
		)

		service, err := Get[LogService](ctx)
		assert.NoError(t, err)

		message := service.Log("hello")
		assert.Equal(t, "h3110", message)
		assert.NoError(t, Close(ctx))
		assert.True(t, printClosed)
		assert.True(t, l33dClosed)
		assert.True(t, logClosed)
	})

	t.Run("add many unsafe providers then get a complex one", func(t *testing.T) {
		defer func() {
			// recover from panic and test that it is not nil
			if r := recover(); r == nil {
				require.Fail(t, "the test should have panic")
			} else {
				t.Logf("Recovered from panic: %v", r)
			}
		}()

		ctx := context.Background()
		ctx = Add(ctx,
			func() (PrintService, error) {
				return NewPrintService(func() {}), nil
			},
			func() (*l33dService, error) {
				return NewL33dService(func() {}), fmt.Errorf("l33dService could not be created")
			},
			func(ctx context.Context, ps PrintService, hs *l33dService) (LogService, error) {
				return NewLogService(ps, hs, func() {}), nil
			},
		)

		_, _ = Get[LogService](ctx)
	})

	t.Run("provide a function then get it", func(t *testing.T) {
		ctx := context.Background()
		ctx = Add(ctx, func() TestFunction {
			return NewTestFunction("hello")
		})
		testFunction := GetOrCrash[TestFunction](ctx)

		assert.Equal(t, "hello", testFunction())
		assert.NoError(t, Close(ctx))
	})
}

func TestConcurrentGet(t *testing.T) {
	for i := 0; i < 1000; i++ {
		ctx := context.Background()
		ctx = Add(ctx,
			func() PrintService {
				return NewPrintService(func() {})
			},
			func() *l33dService {
				return NewL33dService(func() {})
			},
			func(ps PrintService, hs *l33dService) LogService {
				return NewLogService(ps, hs, func() {})
			},
		)

		wg := &sync.WaitGroup{}
		wg.Add(5)

		consume := func() {
			defer wg.Done()
			service, err := Get[LogService](ctx)
			assert.NoError(t, err)
			assert.NotNil(t, service)
		}

		go consume()
		go consume()
		go consume()
		go consume()
		go consume()

		wg.Wait()
	}
}

func TestEmbeddedUsage(t *testing.T) {
	type config bool
	type destination string
	type message string

	expectedDestination := destination("mailbox")
	expectedMessage := message("hello man!")

	ctx := context.Background()
	ctx = Add(ctx,
		func() config {
			return true
		},
		func(ctx context.Context, conf config) destination {
			return expectedDestination
		},
		func(ctx context.Context, conf config) message {
			return expectedMessage
		},
	)

	var err error
	var actualMessage message
	var actualDestination destination

	assert.NoError(t, Consume(ctx, func(ctx context.Context, m message, d destination) {
		actualMessage, err = Get[message](ctx)
		assert.NoError(t, err)
		actualDestination, err = Get[destination](ctx)
		assert.NoError(t, err)
	}))

	assert.NoError(t, err)
	assert.Equal(t, expectedMessage, actualMessage)
	assert.Equal(t, expectedDestination, actualDestination)
}

type TestFunction func() string

func NewTestFunction(value string) TestFunction {
	return func() string {
		return value
	}
}

type PrintService interface {
	io.Closer
	Print(message string) string
}

func NewPrintService(onClose func()) printService {
	return printService{
		onClose: onClose,
	}
}

type printService struct {
	onClose func()
}

func (d printService) Close() error {
	d.onClose()
	return nil
}

func (d printService) Print(message string) string {
	return message
}

type L33dService interface {
	io.Closer
	Leeds(message string) string
}

func NewL33dService(onClose func()) *l33dService {
	return &l33dService{
		mapping: map[rune]rune{
			'e': '3',
			'l': '1',
			't': '7',
			'o': '0',
		},
		onClose: onClose,
	}
}

type l33dService struct {
	mapping map[rune]rune
	onClose func()
}

func (d *l33dService) Close() error {
	d.onClose()
	return nil
}

func (d *l33dService) Leeds(message string) string {
	var result strings.Builder
	for _, original := range message {
		if replacement, present := d.mapping[original]; present {
			result.WriteRune(replacement)
		} else {
			result.WriteRune(original)
		}
	}

	return result.String()
}

type LogService interface {
	io.Closer
	Log(message string) string
}

func NewLogService(printService PrintService, l33dService L33dService, onClose func()) *logService {
	return &logService{
		printService: printService,
		l33dService:  l33dService,
		onClose:      onClose,
	}
}

type logService struct {
	printService PrintService
	l33dService  L33dService
	onClose      func()
}

func (u *logService) Close() error {
	u.onClose()
	return nil
}

func (u *logService) Log(message string) string {
	return u.printService.Print(u.l33dService.Leeds(message))
}

type BlockingService interface {
	BlockFor(duration time.Duration)
}

func NewBlockingService() BlockingService {
	return blockingService{}
}

type blockingService struct {
}

func (b blockingService) BlockFor(duration time.Duration) {
	time.Sleep(duration)
}
