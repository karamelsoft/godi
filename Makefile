VERSION=v0.12.0

download:
	go mod download

tiny:
	go mod tiny

clean:
	go clean -cache -testcache

compile:
	CGO_ENABLED=0 go build -v ./...

test:
	CGO_ENABLED=0 go test -cover ./...

release: test
	git push
	git tag -a $(VERSION) -m "New Release $(VERSION)"
	git push --tags
