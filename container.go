package godi

import (
	"context"
	"io"
	"reflect"
	"sync"
)

type container interface {
	io.Closer
	Get(ctx context.Context) any
}

func newLazyContainer(containers *containers, provider any) *lazyContainer {
	return &lazyContainer{
		containers: containers,
		provider:   provider,
		value:      nil,
		lock:       &sync.Mutex{},
	}
}

type lazyContainer struct {
	containers *containers
	provider   any
	value      any
	lock       *sync.Mutex
}

func (c *lazyContainer) Close() error {
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.value != nil && c.implements((*io.Closer)(nil)) {
		return c.value.(io.Closer).Close()
	}
	return nil
}

func (c *lazyContainer) Get(ctx context.Context) any {
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.value == nil {
		c.value = c.executeProvider(ctx)
	}

	return c.value
}

func (c *lazyContainer) implements(contract any) bool {
	return reflect.TypeOf(c.value).Implements(reflect.TypeOf(contract).Elem())
}

func (c *lazyContainer) executeProvider(ctx context.Context) any {
	results := reflect.ValueOf(c.provider).Call(c.retrieveArguments(ctx))
	if len(results) == 2 {
		if err := results[1].Interface(); err != nil {
			panic(err)
		}
	}

	return results[0].Interface()
}

func (c *lazyContainer) retrieveArguments(ctx context.Context) []reflect.Value {
	var values []reflect.Value
	providerType := reflect.TypeOf(c.provider)
	for i := 0; i < providerType.NumIn(); i++ {
		values = append(values, reflect.ValueOf(c.containers.ExecuteOrCrash(ctx, providerType.In(i))[0]))
	}

	return values
}
