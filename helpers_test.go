package godi

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConsumeProviderValue(t *testing.T) {
	ctx := context.Background()
	ctx = Add(ctx,
		ValueProvider[string]("hello"),
		ValueProvider[int](42),
	)

	assert.Equal(t, "hello", ExecuteOrCrash[string](ctx, ValueConsumer[string]()))
	assert.Equal(t, 42, ExecuteOrCrash[int](ctx, ValueConsumer[int]()))
}
