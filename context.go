package godi

import (
	"context"
	"fmt"
)

type diProvidersKey string

var (
	providersKey diProvidersKey = "di_providers"
)

func Close(ctx context.Context) error {
	diProviders := ctx.Value(providersKey)
	if diProviders != nil {
		return diProviders.(*containers).Close()
	}

	return nil
}

func Add(ctx context.Context, providers ...any) context.Context {
	newCtx := ctx
	diProviders := ctx.Value(providersKey)
	if diProviders == nil {
		diProviders = newContainers()
		newCtx = context.WithValue(ctx, providersKey, diProviders)
	}
	diProviders.(*containers).Add(providers...)

	return newCtx
}

func Get[T any](ctx context.Context) (T, error) {
	return Execute[T](ctx, ValueConsumer[T]())
}

func GetOrCrash[T any](ctx context.Context) T {
	return ExecuteOrCrash[T](ctx, ValueConsumer[T]())
}

func Consume(ctx context.Context, consumer any) error {
	diProviders := ctx.Value(providersKey)
	if diProviders == nil {
		panic(fmt.Errorf("unitialized godi context"))
	}

	_, err := diProviders.(*containers).Execute(ctx, consumer)

	return err
}

func ConsumeOrCrash(ctx context.Context, consumer any) {
	if err := Consume(ctx, consumer); err != nil {
		panic(err)
	}
}

func Execute[T any](ctx context.Context, consumer any) (T, error) {
	var result T

	diProviders := ctx.Value(providersKey)
	if diProviders == nil {
		panic(fmt.Errorf("unitialized godi context"))
	}

	values, err := diProviders.(*containers).Execute(ctx, consumer)
	if err != nil {
		return result, err
	}

	if values[0] != nil {
		result = values[0].(T)
	}

	return result, nil
}

func ExecuteOrCrash[T any](ctx context.Context, consumer any) T {
	result, err := Execute[T](ctx, consumer)
	if err != nil {
		panic(err)
	}

	return result
}
